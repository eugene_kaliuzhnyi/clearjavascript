var clock = (function() {
    var DOMElements = null;
    var alarmArr = [];
    var index = 0;

    function fixTimeFormat(time) {
        return (time < 10) ? "0" + time : time;
    }

    function startClock() {
        var today = new Date();
        var hours = fixTimeFormat(today.getHours());
        var minutes = fixTimeFormat(today.getMinutes());
        var seconds = fixTimeFormat(today.getSeconds());
        DOMElements.clock.innerHTML = `${hours} : ${minutes} : ${seconds}`;
        if(seconds == "00") {
            checkAlarm(hours, minutes);
        }
    }
    function checkAlarm(hours, minutes) {
        showHideContent("hide", DOMElements.alarmMsg);
        var isAlarmAvailable = alarmArr.some(function(item){
            return item.time === `${hours}:${minutes}`;
        });
        isAlarmAvailable && showHideContent("show", DOMElements.alarmMsg);
    }
    function addAlarm() {
        if(DOMElements.inputTime.value &&
            validateTime(DOMElements.inputTime.value) &&
            !checkAlarmExistence(DOMElements.inputTime.value)){
            alarmArr.push({
                id: index,
                time: DOMElements.inputTime.value
            });
            printTableLine(alarmArr[alarmArr.length -1]);
            showHideContent("show", DOMElements.alarmTableArea);
            DOMElements.inputTime.value = "";
            index++;
        } else {
            showHideContent("show", DOMElements.errorMsg);
            setTimeout(function(){showHideContent("hide", DOMElements.errorMsg)}, 5000);
        }
    }

    function validateTime(time) {
        var regex = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
        return regex.test(time);
    }

    function checkAlarmExistence(time) {
        for (var i = 0; i < alarmArr.length; i++){
            if(alarmArr[i].time === time){
                return true;
            }
        }
        return false;
    }

    function printTableLine(obj) {
        var row = `<tr data-index="${obj.id}">\
                    <td class="alarm-counter">${alarmArr.length}</td>\
                    <td>${obj.time}</td>\
                     <td><button>Удалить</button></td>\
                </tr>`;
        DOMElements.alarmTable.innerHTML += row;
    }
    function showHideContent(status, elem){
        (status == "show") ? elem.classList.remove("hide") : elem.classList.add("hide");
    }

    function removeAlarm(event) {
        if(event.target.tagName != "BUTTON"){return}
        var tr = event.target.closest("tr");
        var index = parseInt(tr.getAttribute("data-index"));
        var elemsToDelete = alarmArr.filter(function(obj) {
            return obj.id === index;
        });
        if(elemsToDelete[0]) {
            DOMElements.alarmTable.removeChild(tr);
            alarmArr.splice(alarmArr.indexOf(elemsToDelete[0]), 1);
            updateRowNumbers();
        }
        if(alarmArr.length < 1) {
            showHideContent("hide", DOMElements.alarmTableArea);
        }

    }
    function updateRowNumbers() {
        var countArray = DOMElements.alarmTable.getElementsByClassName("alarm-counter");
        var count = 1;
        for(var i = 0; i < countArray.length; i++){
            countArray[i].innerHTML = count;
            count++;
        }
    }
    function initListeners() {
        DOMElements.alarmBtn.addEventListener("click", addAlarm);
        DOMElements.alarmTable.addEventListener("click", removeAlarm);
    }
    return {
        setAlarm: function(form){
            DOMElements = form;
        },
        initAlarm: function(){
            startClock();
            setInterval(startClock, 1000);
            initListeners();
        }
    }
})();
clock.setAlarm({
    clock: document.querySelector("#clock"),
    alarmBtn: document.querySelector("#alarmBtn"),
    inputTime: document.querySelector("#inputTime"),
    alarmTable: document.querySelector("#table-content"),
    alarmTableArea: document.querySelector("#alarmTable"),
    alarmMsg: document.querySelector("#alarmMsg"),
    errorMsg: document.querySelector("#validateMsg")
});
clock.initAlarm();