var formValidator = (function () {

    var DOMElements = null;
    var formArr = [];
    var index = 0;

    function validate(event) {
        event.preventDefault();
        if (DOMElements.name.value
            && validateEmail(DOMElements.email.value)
            && DOMElements.password.value) {
            showContentAccordingToStatus('success');
            addNewTableLine();
        } else {
            showContentAccordingToStatus('error')
        }
    }
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function showContentAccordingToStatus(status) {
        var formcontainer = DOMElements.container;
        formcontainer.classList = "";
        formcontainer.classList.add(status);
    }

    function addNewTableLine() {
        var newItem = {
            name: DOMElements.name.value,
            email: DOMElements.email.value,
            password: DOMElements.password.value
        };
        formArr.push(newItem);
    	printTableLine(formArr[formArr.length - 1]);
        index++;
    }
    function printTableLine(){
        var row = `<tr data-index="${index}">\
                    <td class="index">${index+1}</td>\
                    <td>${DOMElements.name.value}</td>\
                    <td>${DOMElements.email.value}</td>\
                    <td class="password-cell">\
                        ${convertCharactersToAsterisks(DOMElements.password.value)}
                    </td>\
                    <td><button class="show-password" data-status="hide">Показать пароль</button></td>\
                </tr>`;
        DOMElements.tableContent.innerHTML += row;
    }

    function convertCharactersToAsterisks (string) {
        var tranformedString = "";
        for(var i = 0; i < string.length; i++){
            tranformedString += '*';
        }
        return tranformedString;
    }
    function reset (event) {
        event.preventDefault();
        clearForm();
        showContentAccordingToStatus("fill");
    }
    function clearForm (){
        DOMElements.name.value = "";
        DOMElements.email.value = "";
        DOMElements.password.value = "";
    }

    function showPassword(event){
        if(event.target.tagName != "BUTTON"){return};
        var tr = event.target.closest("tr");
        var index = tr.getAttribute("data-index");
        var passwordCell = tr.getElementsByClassName("password-cell")[0];
        var passwordButton = tr.getElementsByClassName("show-password")[0];
        var passwordStatus = passwordButton.getAttribute("data-status");
        if (passwordStatus == 'hide'){
            passwordCell.innerHTML = formArr[index].password;
            passwordButton.innerHTML = "Спрятать пароль";
            passwordButton.setAttribute("data-status", "show");
        } else if (passwordStatus == "show") {
            passwordCell.innerHTML = convertCharactersToAsterisks(formArr[index].password);
            passwordButton.innerHTML = "Показать пароль";
            passwordButton.setAttribute("data-status", "hide");
        }
    }

    function initListeners() {
        DOMElements.submitBtn.addEventListener("click", validate);
        DOMElements.resetBtn.addEventListener("click", reset);
        DOMElements.tableContent.addEventListener("click", showPassword);
    }

    return {
        setFormData: function (form) {
            DOMElements = form;
        },
        initValidator: function () {
            initListeners();
        }
    }

}());

formValidator.setFormData({
    name: document.querySelector("#inputName"),
    email: document.querySelector("#inputEmail"),
    password: document.querySelector("#inputPassword"),
    tableContent: document.querySelector("#table-content"),
    submitBtn: document.querySelector("#submit"),
    resetBtn: document.querySelector("#reset"),
    container: document.querySelector("#formArea")
});
formValidator.initValidator();