var gallery = (function() {
    var galleryObj = null;
    var data = [];
    var editData = [];
    var amountImages = 0;
    var transformeData = [];

    function getData(){
        $.get( "js/gallery.json", function(array) {
            data = array;
            editData = transformeArr(data);
        })
    }
    function capitalizeFirstLetter(word) {
        return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase();
    }

    function addProtocol(adress) {
        return "http://" + adress;
    }

    function truncateString(string) {
        return string.substring(0, 15) + "...";
    }

    function formatDate(date) {
        var tmpDate = new Date(date);
        var convertedDate = {
            year: tmpDate.getFullYear(),
            month: tmpDate.getMonth() + 1,
            date: tmpDate.getDate(),
            hours: tmpDate.getHours(),
            minutes: tmpDate.getMinutes()
        };
        for (var key in convertedDate) {
            convertedDate[key] = (convertedDate[key] < 10) ? "0" + convertedDate[key] : convertedDate[key];
        }
        return convertedDate.year + "/" + convertedDate.month + "/" + convertedDate.date + " " + convertedDate.hours + ":" +
            convertedDate.minutes;
    }

    function editParams(firstPar, secondPar) {
        return String(firstPar) + " => " + String(secondPar);
    }

    function transformeArr(arr) {
        return arr.map(function (item) {
            return {
                id: item.id,
                name: capitalizeFirstLetter(item.name),
                url: addProtocol(item.url),
                description: truncateString(item.description),
                date: formatDate(item.date),
                params: editParams(item.params.status, item.params.progress)
            };
        });
    }

    function printWithInterpolation(item, row) {
        var resultHTML = `<div class="col-sm-3 col-xs-6 item-wrapper" data-id="${item.id}">\
                            <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                            <div class="info-wrapper">\
                                <div class="text-muted">${item.name}</div>\
                                <div class="text-muted">${item.description}</div>\
                                <div class="text-muted">${item.params}</div>\
                                <div class="text-muted">${item.date}</div>\
                            </div>\
                        <button class="btn btn-danger">Удалить</button>\
                        <div>`;
        row.innerHTML += resultHTML;
    }

    function printElementInGallery() {
        if (transformeData.length < editData.length) {
            for(var i = 0; i < editData.length; i++) {
                var obj = editData[i];
                if(!isAlreadyShown(obj.id)) {
                    printWithInterpolation(obj, galleryObj.galleryArea);
                    transformeData.push(obj);
                    amountImages++;
                    galleryObj.countImages.innerHTML = amountImages;
                    showHideContent("hide", galleryObj.successMsg);
                    return;
                }
            }
        }
        else {
            alert("Больше элементов нет!");
        }
    }

    function isAlreadyShown(id) {
        var exist = transformeData.some(function(obj){
            return obj.id === id;
        });
        return exist;
    }

    function removeElementFromGallery(event) {
        var galElement = event.target.closest('.item-wrapper');
        var elemId = parseInt(galElement.getAttribute("data-id"));
        galleryObj.galleryArea.removeChild(galElement);
        transformeData.splice(transformeData.indexOf(editData[elemId]), 1);
        amountImages = amountImages - 1;
        galleryObj.countImages.innerHTML = amountImages;
        showHideContent("hide", galleryObj.successMsg);
    }
    function saveGalleryArray(){
        $.post( "api/save", {data: transformeData}, function( data ) {
            if(data){
                showHideContent("show", galleryObj.successMsg);
            }
        })
    }
    function showHideContent(status, element){
        if(status == "show"){
            element.classList.remove("hide");
        } else {
            element.classList.add("hide");
        }
    }
    function initlisteners(){
        galleryObj.addImage.addEventListener("click", printElementInGallery);
        galleryObj.galleryArea.addEventListener("click", removeElementFromGallery);
        galleryObj.saveGallery.addEventListener("click", saveGalleryArray);
    }
    return {
        setGallery: function(gallery){
            getData();
            galleryObj = gallery;
        },
        initGallery: function(){
            initlisteners();
        }
    }
})();
gallery.setGallery({
    addImage: document.querySelector("#add"),
    galleryArea: document.querySelector("#result"),
    countImages: document.querySelector("#count"),
    saveGallery: document.querySelector("#save"),
    successMsg: document.querySelector(".bg-success")
});
gallery.initGallery();

