function  FormValidator(form) {

    var DOMElements = null;
    var formArr = [];
    setFormData(form);

    function validate(event) {
        event.preventDefault();
        if (DOMElements.name.value
            && validateEmail(DOMElements.email.value)
            && DOMElements.password.value) {
            showContentAccordingToStatus.call(this,'success');
            addNewTableLine.call(this);
        } else {
            showContentAccordingToStatus.call(this, 'error');
        }
    }
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function showContentAccordingToStatus(status) {
        var formcontainer = DOMElements.container;
        formcontainer.classList = "";
        formcontainer.classList.add(status);
    }

    function addNewTableLine() {
        var newItem = {
            name: DOMElements.name.value,
            email: DOMElements.email.value,
            password: DOMElements.password.value
        };
        formArr.push(newItem);
    	printTableLine.call(this, formArr[formArr.length - 1]);
    }
    function printTableLine(){
        var row = `<tr data-index="${formArr.length - 1}">\
                    <td class="index">${formArr.length}</td>\
                    <td>${DOMElements.name.value}</td>\
                    <td>${DOMElements.email.value}</td>\
                    <td class="password-cell">\
                        ${convertCharactersToAsterisks(DOMElements.password.value)}
                    </td>\
                    <td><button class="show-password" data-status="hide">Показать пароль</button></td>\
                </tr>`;
        DOMElements.tableContent.innerHTML += row;
    }

    function convertCharactersToAsterisks (string) {
       return string.replace(/./g, '*');
    }
    function reset (event) {
        event.preventDefault();
        clearForm.call(this);
        showContentAccordingToStatus.call(this, "fill");
    }
    function clearForm (){
        DOMElements.name.value = "";
        DOMElements.email.value = "";
        DOMElements.password.value = "";
    }

    function showPassword(event){
        if(event.target.tagName != "BUTTON"){return};
        var tr = event.target.closest("tr");
        var index = tr.getAttribute("data-index");
        var passwordCell = tr.getElementsByClassName("password-cell")[0];
        var passwordButton = tr.getElementsByClassName("show-password")[0];
        var passwordStatus = passwordButton.getAttribute("data-status");
        if (passwordStatus == 'hide'){
            passwordCell.innerHTML = formArr[index].password;
            passwordButton.innerHTML = "Спрятать пароль";
            passwordButton.setAttribute("data-status", "show");
        } else if (passwordStatus == "show") {
            passwordCell.innerHTML = convertCharactersToAsterisks(formArr[index].password);
            passwordButton.innerHTML = "Показать пароль";
            passwordButton.setAttribute("data-status", "hide");
        }
    }

    function initListeners() {
        DOMElements.submitBtn.addEventListener("click", validate.bind(this));
        DOMElements.resetBtn.addEventListener("click", reset.bind(this));
        DOMElements.tableContent.addEventListener("click", showPassword.bind(this));
    }
    function setFormData(form) {
        DOMElements = form;
    }
    this.initValidator = function () {
        initListeners.call(this);
    };
}

var form = new FormValidator({
    name: document.querySelector("#inputName"),
    email: document.querySelector("#inputEmail"),
    password: document.querySelector("#inputPassword"),
    tableContent: document.querySelector("#table-content"),
    submitBtn: document.querySelector("#submit"),
    resetBtn: document.querySelector("#reset"),
    container: document.querySelector("#formArea")
});
form.initValidator();
