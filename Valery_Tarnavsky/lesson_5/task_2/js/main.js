﻿var btn     = document.getElementById("play"),
    player1 = document.getElementById("player1"),
    player2 = document.getElementById("player2"),
    result  = document.getElementById("result");

function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}

function getNameById(number){
    var name;
    switch (number){
        case 1:
            name = "камень";
            break;
        case 2:
            name = "ножницы";
            break;
        default:
            name = "бумага";
    }
    return name;
}

function insertIcon(number){
    var icon;
    if (number === 1){
        icon = '<i class="fa fa-hand-rock-o" aria-hidden="true"></i>';
    }else if (number === 2){
        icon = '<i class="fa fa-hand-scissors-o" aria-hidden="true"></i>';
    }else {
        icon = '<i class="fa fa-hand-paper-o" aria-hidden="true"></i>';
    }
    return icon;
}

function determineWinner (number1, number2){
    var winner;
    var subtraction = number1 - number2;
    if (subtraction === 0){
        winner = 0;
    }else if (subtraction === -1 || subtraction === 2){
        winner = 1;
    }else {
        winner = 2;
    }
    return winner;
}

function printResult(winner) {
    if (winner === 1) {
        result.innerHTML = "Выиграл <u>первый</u> игрок";
    } else if (winner === 2) {
        result.innerHTML = "Выиграл <u>второй</u> игрок";
    } else {
        result.innerHTML = "Ничья";
    }
}

function runGame() {
    var playerResult1 = getPlayerResult();
    var playerResult2 = getPlayerResult();
    player1.innerHTML = insertIcon(playerResult1);
    player2.innerHTML = insertIcon(playerResult2);
    player1.innerHTML += getNameById(playerResult1);
    player2.innerHTML += getNameById(playerResult2);
    printResult(determineWinner (playerResult1, playerResult2));
}

btn.addEventListener("click", runGame);