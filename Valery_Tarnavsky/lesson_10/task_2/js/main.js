function FormValidator(form){
    var DOMElements = form;
    var dataList = [];
    var regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    function toggleContent (elem, status) {
        elem.forEach(function (item) {
            status == "show" ? item.classList.remove("hide") : item.classList.add("hide");
        });
    }

    function addNewTableLine (arr, count){
        var row = '<tr>\
                        <td>'+ count +'</td>\
                        <td>'+ arr.name +'</td>\
                        <td>'+ arr.email +'</td>\
                        <td class="pass">'+ replacePass.call(this, count) +'</td>\
                        <td><a class="btn btn-primary btn-sm replaceBtn" data-visibility="hidden" data-target="'+ count +'">Показать пароль</a></td>\
                   </tr>';
        DOMElements.tableContent.innerHTML += row;
    }

    function createDataList(){
        dataList.push({
            name : DOMElements.form.name.value,
            email : DOMElements.form.email.value,
            password : DOMElements.form.password.value
        });
        addNewTableLine.call(this, dataList[dataList.length-1], dataList.length);
    }

     function validate (event){
        event.preventDefault();
        var emailTest = regExp.test(DOMElements.form.email.value);
        var check = true;
        for (var key in DOMElements.form) {
            if (DOMElements.form[key].value == "") {check = false;}
        }
        if (check && emailTest){
            toggleContent.call(this, [DOMElements.successMsg,DOMElements.tableContainer], "show");
            toggleContent.call(this,[DOMElements.errorMsg, DOMElements.formContainer], "hide");
            createDataList.call(this);
        }else{
            toggleContent.call(this, [DOMElements.errorMsg], "show");
            toggleContent.call(this, [DOMElements.successMsg], "hide");
        }
    }

    function replacePass (number) {
        return dataList[number-1].password.replace(/./g, '*');
    }

    function showUserPass (number) {
        return dataList[number-1].password;
    }

    function togglePassword (event){
        var target = event.target;
        if (target.tagName != 'A') {return}
        var parentRow = target.closest("TR");
        var passElem = parentRow.getElementsByClassName("pass")[0];
        var numberElem = target.getAttribute("data-target");
        if(target.getAttribute("data-visibility") == "hidden"){
            target.setAttribute("data-visibility", "visible");
            target.innerHTML = "Скрыть пароль";
            passElem.innerHTML = showUserPass.call(this, numberElem);
        }else{
            target.setAttribute("data-visibility", "hidden");
            target.innerHTML = "Показать пароль";
            passElem.innerHTML = replacePass.call(this, numberElem);
        }
    }

    function reset() {
        toggleContent.call(this, [DOMElements.formContainer], "show");
        toggleContent.call(this, [DOMElements.errorMsg, DOMElements.successMsg, DOMElements.tableContainer], "hide");
        for (var key in DOMElements.form) {
            DOMElements.form[key].value = "";
        }
    }

    function initListeners() {
        DOMElements.submitBtn.addEventListener("click", validate.bind(this));
        DOMElements.tableContent.addEventListener("click", togglePassword.bind(this));
        DOMElements.resetBtn.addEventListener("click", reset.bind(this));
    }

    this.initValidator = function(){
        initListeners.call(this);
    };
}

formValidator = new FormValidator({
    successMsg: document.querySelector(".bg-success"),
    errorMsg: document.querySelector(".bg-danger"),
    formContainer: document.querySelector("#form"),
    form : {
        name: document.querySelector("#inputName"),
        email: document.querySelector("#inputEmail"),
        password: document.querySelector("#inputPassword")
    },
    tableContainer: document.querySelector("#tableContainer"),
    tableContent: document.querySelector("#tableContent"),
    submitBtn: document.querySelector("#submit"),
    resetBtn: document.querySelector("#reset")
});
formValidator.initValidator();


