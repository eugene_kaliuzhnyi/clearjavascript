var formValidator = (function(){
    var DOMElements = null;
    var dataList = [];

	function toggleContent(status){
        DOMElements.successMsg.classList.toggle("hidden", !status);
        DOMElements.errorMsg.classList.toggle("hidden", status);
        DOMElements.tableContainer.classList.toggle("hidden", !status);
        DOMElements.formContainer.classList.toggle("hidden", status);
    }

    function hideMsg(){
        DOMElements.successMsg.classList.toggle("hidden",true);
        DOMElements.errorMsg.classList.toggle("hidden", true);
    }

    function initListeners() {
        DOMElements.submitBtn.addEventListener("click", validate);
        DOMElements.tableContent.addEventListener("click", togglePassword);
        DOMElements.resetBtn.addEventListener("click", reset);

    }

    function validate(event){
        event.preventDefault();
        var regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var emailTest = regExp.test(DOMElements.form.email.value);
        var check = true;
        for (var key in DOMElements.form) {
            if (!DOMElements.form[key].value) {check = false;}
        }
        if (check && emailTest){
            toggleContent(check);
            createDataList();
        }else{
            toggleContent(check);
        }
    }

    function replacePass(number) {
        return dataList[number-1].password.replace(/./g, '*');
    }

    function showUserPass (number) {
        return dataList[number-1].password;
    }

    function createDataList(){
        dataList.push({
            name : DOMElements.form.name.value,
            email : DOMElements.form.email.value,
            password : DOMElements.form.password.value
        });
        addNewTableLine(dataList[dataList.length-1], dataList.length);

    }

    function addNewTableLine(arr, count){
        var row = '<tr>\
                        <td>'+ count +'</td>\
                        <td>'+ arr.name +'</td>\
                        <td>'+ arr.email +'</td>\
                        <td class="pass">'+ replacePass(count) +'</td>\
                        <td><a class="btn btn-primary btn-sm replaceBtn" data-visibility="hidden" data-target="'+ count +'">Показать пароль</a></td>\
                   </tr>';
        DOMElements.tableContent.innerHTML += row;
    }

    function togglePassword(event){
        var target = event.target;
        if (target.tagName != 'A') {return}
        var parentRow = target.closest("TR");
        var passElem = parentRow.getElementsByClassName("pass")[0];
        var numberElem = target.getAttribute("data-target");
        if( target.getAttribute("data-visibility") == "hidden"){
            target.setAttribute("data-visibility", "visible");
            target.innerHTML = "Скрыть пароль";
            passElem.innerHTML = showUserPass(numberElem);
        }else{
            target.setAttribute("data-visibility", "hidden");
            target.innerHTML = "Показать пароль";
            passElem.innerHTML = replacePass(numberElem);
        }
    }

    function reset() {
        toggleContent(false);
        hideMsg();
        for (var key in DOMElements.form) {
            DOMElements.form[key].value = "";
        }
    }

    return {
        setFormData : function(form){
            DOMElements = form;
        },
        initValidator: function(){
            initListeners();
        }
    }
}());

formValidator.setFormData({
    successMsg: document.querySelector(".bg-success"),
    errorMsg: document.querySelector(".bg-danger"),
    formContainer: document.querySelector("#form"),
    form : {
        name: document.querySelector("#inputName"),
        email: document.querySelector("#inputEmail"),
        password: document.querySelector("#inputPassword")
    },
    tableContainer: document.querySelector("#tableContainer"),
    tableContent: document.querySelector("#tableContent"),
    submitBtn: document.querySelector("#submit"),
    resetBtn: document.querySelector("#reset")
});
formValidator.initValidator();


