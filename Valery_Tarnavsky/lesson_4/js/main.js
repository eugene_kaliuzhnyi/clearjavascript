var total = 0;
var resultElement  = document.getElementById("result");
for (var i = 0; i <= 15; i++){
    var first = Math.floor((Math.random() * 6) + 1);
    var second = Math.floor((Math.random() * 6) + 1);

    if (i == 8 || i == 13){continue;}

    resultElement.innerHTML += '<span class="count">' + i + '</span><br />';
    resultElement.innerHTML += "Первая кость: <span>" + first +"</span>. " + "Вторая кость: <span>" + second + "</span><br />";

    if (first == second){
        resultElement.innerHTML += "Выпал дубль. Число: " + first+ "<br />";
    }

    if (first < 3 && second > 4){
        var subtraction = second - first;
        resultElement.innerHTML += "Большой разброс между костями. Разница составляет: " + subtraction + "<br />"
    }

    total += first + second;
    resultElement.innerHTML += "Сумма составляет: " + total + "<br />";
}

var result = total > 100 ? "Победа! Cумма Ваших очков: " + total : "Вы проиграли, сумма Ваших очков: " + total;
resultElement.innerHTML += '<div class="total">' + result + '</div>';



