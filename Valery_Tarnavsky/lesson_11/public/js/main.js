﻿var gallery = (function(){
    var elems            = null;
    var visibleCount     = 0;
    var arrayCount       = 0;
    var transformedArray = [];

    function capitalizeFirstLetter(s) {
        return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
    }

    function cutString (s){
        return s.length > 15 ? s.slice(0, 15) + "..." : s;
    }

    function formatDate(date){
        var tmpDate = new Date(date);
        var fixDate = {
            year    : tmpDate.getFullYear(),
            month   : tmpDate.getMonth() + 1,
            day     : tmpDate.getDate(),
            hours   : tmpDate.getHours(),
            minutes : tmpDate.getMinutes()
        };
        for (var k in fixDate) {
            fixDate[k] = fixDate[k] < 10  ? ('0'+ fixDate[k]) : fixDate[k];
        }
        return fixDate.year + "/" + fixDate.month + "/" + fixDate.day + " " +  fixDate.hours + ":" + fixDate.minutes;
    }

    function count(){
        elems.count.innerHTML = visibleCount;
    }

    function toggleContent (elem, status) {
        elem.forEach(function (item) {
            status == "show" ? item.classList.remove("hide") : item.classList.add("hide");
        });
    }

    function print(tmp, elem){
        elem.innerHTML += tmp;
    }

    function removeItem (event){
        var target = event.target;
        if (target.tagName != 'BUTTON') {return}
        target.closest("div").remove();
        visibleCount--;
        count();
        toggleContent([elems.errorMsg], 'hide');
        toggleContent([elems.successMsg], 'hide');
    }

    function saveGallery (){
        var data = JSON.stringify(transformedArray);
        $.post("../save.php",  {json: data}).done(function() {
            toggleContent([elems.successMsg], 'show');
            toggleContent([elems.errorMsg], 'hide');
        });
    }

    function transformArray (arr){
        return arr.map(function(item) {
            return transformedArray.push({
                id         : item.id,
                url        : "http://" + item.url,
                name       : capitalizeFirstLetter(item.name),
                params     : item.params.status + "=>" + item.params.progress,
                description: cutString(item.description),
                date       : formatDate(item.date)
            });
        });
    }

    function interpolateItems(item){
        var itemTemplate = '<div class="col-sm-3 col-xs-6 item">\
            <img src="'+item.url+'" alt="'+item.name+'" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">'+item.name+'</div>\
                    <div class="text-muted">'+item.description+'</div>\
                    <div class="text-muted">'+item.params+'</div>\
                    <div class="text-muted">'+item.date+'</div>\
                </div>\
                <button class="btn btn-danger" data-target="'+item.id+'">Удалить</button>\
            </div>';
        var result = "";
        result += itemTemplate;
        return result;
    }

    function parseData (data) {
        data.forEach(function(item){
            item.params = (JSON.parse(item.params));
        });
        transformArray(data);
    }

    function runGallery() {
        if(visibleCount < transformedArray.length){
            print(interpolateItems(transformedArray[arrayCount]), elems.result);
            visibleCount++;
        }else{toggleContent([elems.errorMsg], 'show')}
        arrayCount < transformedArray.length-1 ? arrayCount++ : arrayCount = 0;
        count();
        toggleContent([elems.successMsg], 'hide');
    }

    function initListeners() {
        elems.addButton.addEventListener("click", runGallery);
        elems.result.addEventListener("click", removeItem);
        elems.save.addEventListener("click", saveGallery);
    }

    return {
        setGalleryData : function(gallery){
            elems = gallery;
        },
        initGallery: function(){
            var promise = $.getJSON( "../get.php");
            promise.done(function(data) {
                parseData(data);
                initListeners();
            });
            promise.fail(function() {
                console.log('Something went wrong');
            });
        }
    }
}());

gallery.setGalleryData({
    result     :  document.querySelector('#result'),
    addButton  :  document.querySelector('#add'),
    count      :  document.querySelector('#count'),
    save       :  document.querySelector('#save'),
    successMsg : document.querySelector('.success'),
    errorMsg   : document.querySelector('.error')
});

gallery.initGallery();
