/*  1.
 Создать главную функцию run() в которой будет выполняться основной код (цикл)
 Так же эта функция должна содержать в себе вызовы всех остальных функций.

 2.
 Сделать функцию для получения случайных чисел (getRndNumber).
 Значение каждой переменной, в которую мы записываем,
 какая выпала кость получать с помощью вызова этой функции

 3.
 Сделать одну функцию которая будет печатать строки (print).
 Она должна принимать только один аргумент. Строку текста которую надо напечатать.
 (если у вас выводите данные не только в div с id result а возможно еще в какой то другой div,
 тогда функция должна принимать 2 аргумента: id и Строку)

 4.
 Сделать функцию для определения совпадений. (isNumbersEqual).
 Она должна содержать в себе проверку на совпадение и внутри себя
 вызывать функцию для печать данных в HTML (print)

 5.
 Сделать функцию для определения разницы. (isBigDifference).
 Она должна содержать в себе соответствующую проверку и внутри себя
 вызывать функцию для печать данных в HTML (print)

 6.
 Сделать функцию для вычисления результата total.
 Функция должна вычислить результат и вернуть его.
 То есть вернуть строку. Полученное из функции значение необходимо потом напечатать
 с помощью функции (print)
 */
"use strict";

var idResult = document.getElementById("result");

function getRndNumber() {
    return Math.floor((Math.random() * 6) + 1);
}

function printTextInResult(text) {
    idResult.innerHTML += text;
}

function isNumbersEqual(first, second) {
    if (first == second) {
        printTextInResult(`Выпал дубль. Число ${first} <br>`);
    }
}

function isBigDifference(first, second) {
    if (first < 3 && second > 4) {
        var diff = second - first
        printTextInResult(`Большой разброс между костями. Разница составляет ${diff} <br>`);
    }
}

function getFinalResult(total) {
    return (total > 100) ? `Победа, вы набрали ${total} очков` : `Вы проиграми, у вас ${total} очков`

}

(function run () {
    for (var i = 1; i <= 15; i++) {

        if (i == 8 || i == 13) {
            continue;
        }
        var first = getRndNumber(),
            second = getRndNumber(),
            total=0;

        printTextInResult(`Первая кость: ${first} Вторая кость: ${second} <br>`);

        isNumbersEqual(first, second);
        isBigDifference(first, second);
        total += first + second;
    }

    printTextInResult("<hr>" + getFinalResult(total));
})();


