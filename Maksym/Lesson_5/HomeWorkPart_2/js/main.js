/*У вас есть 2 игрока которые играют в игру. У каждого может выпасть камень, ножницы или бумага.
 На самом деле у вас есть функция (getPlayerResult) которая возвращает случайные числа от 1 до 3
 1 — камень
 2 — ножницы
 3 — бумага
 В заготовке реализован следующий функционал.
 По нажатии на кнопку получаем случайное число и выводим его в соответствующий div элемент.

 1.
 Вместо того чтоб выводить на екран случайное число как в примере вам необходимо
 добавить функцию (getNameById) которая будет принимать это число и возвращать слово «камень», «ножницы», или «бумага»
 согласно словарю указанному выше.

 2.
 На экран вывести полученную текстовую строку для каждого из игроков.

 3.
 Написать функцию (deterinmeWinner), которая будет принимать два числа, предварительно полученные в функции getPlayerResult и принимать решение который из игроков выиграл.

 4.
 Результатом выполнения функции determineWinner должно быть число, номер игрока, который выиграл.
 То есть эта функция должна возвращать номер игрока который выиграл

 5.
 Функция printResult должна принять номер игрока, который выиграл и напечатать в div Id result текстовое сообщение типа: «выиграл первый игрок» номер игрока надо вывести словами.
*/
"use strict"
var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var idResult = document.getElementById("result");


function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}

function showTextInResult(text) {
    idResult.innerHTML = text;
}

function getNameById(playerHand) {
    var hand;
    switch (playerHand) {
        case 1:
            hand = `Камень`;
            break;
        case 2:
            hand = `Ножницы`;
            break;
        case 3:
            hand = `Бумага`;
            break;
        default:
            hand = `А если подумать?`;
    }
    return hand;
}

function getWiner(player1Hand, player2Hand) {
    var winner;
    var battle = +(String(player1Hand) + String(player2Hand));
    switch (battle) {
        case 12:
        case 31:
        case 23:
            winner = 1;
            break;
        case 21:
        case 13:
        case 32:
            winner = 2;
            break;
        case 11:
        case 22:
        case 33:
            winner = 0;
            break;
        default:
            showTextInResult("Это фейл");
    }
    return winner;
}

function printResult(winner) {
    switch (winner) {
        case 1:
            showTextInResult(`<span class="alert alert-success"> Выиграл первый игрок </span>`);
            break;
        case 2:
            showTextInResult(`<span class="alert alert-success"> Выиграл второй игрок </span>`);
            break;
        case 0:
            showTextInResult(`<span class="alert alert-success"> Победила дружба </span>`);
            break;
        default:
            showTextInResult(`<span class="alert alert-danger"> Здесь был батл </span>`);
    }

}

function runGame() {
    var player1Hand = getPlayerResult();
    var player2Hand = getPlayerResult();
    player1.innerHTML = getNameById(player1Hand);
    player2.innerHTML = getNameById(player2Hand);
    printResult(getWiner(player1Hand, player2Hand));
}
btn.addEventListener("click", runGame);


