var total = 0;
var resultElement = document.getElementById("result");

for (i = 1; i <= 15; i++){
    var first = Math.floor((Math.random()*6)+1);
    var second = Math.floor((Math.random()*6)+1);
   
    if (i == 8 || i == 13){continue};
    
    resultElement.innerHTML += "Первая кость: " + first + "Вторая кость: " + second + "<br>";
    
    if (first == second){
        resultElement.innerHTML += "Выпал дубль. Число " + first + "<br>";
    };
    
    if (first < 3 && second > 4){
        resultElement.innerHTML += "Большой разброс между костями. Разница составляет" + (second - first) + "<br>";
    };
    
    total += first + second;
};

resultElement.innerHTML += total > 100 ? "Победа, Вы набрали " + total + " очков" : "Вы проиграли, у Вас " + total + " очков";