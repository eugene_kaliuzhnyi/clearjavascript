'use strict';
function FormValidator (form) {
    
    var DOMElements = null;
    var data = [];
    
    setFormData(form);

    function showHideContent (action, items) {
        items.forEach(function (item) {
            if (action == "show") {
                item.classList.remove("hidden");
            } else if (action == "hide") {
                item.classList.add("hidden");
            }
        })
    }

    function resetForm () {
        DOMElements.name.value = DOMElements.email.value = DOMElements.password.value = "";
        showHideContent.call(this, "hide", [DOMElements.errorMsg, DOMElements.successMsg, DOMElements.table.parentNode]);
        showHideContent.call(this, "show", [DOMElements.form]);
    }

    function convertPassToStar (pass) {
        return pass.replace(/[\w \W]/g, '*');
    }

    function convertPass (event) {
        var target = event.target;
        if (!(target.nodeName == "BUTTON")) {
            return false;
        }
        var mode = target.getAttribute("data-password");
        var tdPassword = target.closest("td").previousElementSibling;
        var rowId = (target.getAttribute("data-id") - 1);

        if (mode == "hidden") {
            tdPassword.innerHTML = data[rowId].pass;
            target.setAttribute("data-password", "showed");
            target.textContent = "Скрыть пароль";

        } else if (mode == "showed") {
            tdPassword.innerHTML = data[rowId].passInStar;
            target.setAttribute("data-password", "hidden");
            target.textContent = "Показать пароль";
        }
    }

    function renderRow ({ name, email, pass, passInStar, idRow }) {
        var row = `<tr>
                        <th scope="row">${idRow}</th>
                        <td>${name}</td>
                        <td>${email}</td>
                        <td>${passInStar}</td>
                        <td class="showHidePass"><button class="btn btn-default" data-id="${idRow}" data-password="hidden">Показать пароль</button></td>
                   </tr>`
        showHideContent.call(this, "hide", [DOMElements.form]);
        DOMElements.tableContent.innerHTML += row;
    }

    function addNewTableLine () {
        var dataLength = data.push({
                                    name: DOMElements.name.value,
                                    email: DOMElements.email.value,
                                    pass: DOMElements.password.value,
                                    passInStar: convertPassToStar.call(this, DOMElements.password.value),
                                    idRow: (data.length + 1)    
                                });
        renderRow.call(this, data[dataLength - 1]);
    }

    function validateEmail(email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    function validate (event) {
        event.preventDefault();
        if (DOMElements.name.value &&
            validateEmail.call(this, DOMElements.email.value) &&
            DOMElements.password.value) {

            showHideContent.call(this, "show", [DOMElements.successMsg]);
            showHideContent.call(this, "show", [DOMElements.table.parentNode]);
            addNewTableLine.call(this);
        } else {
            
            showHideContent.call(this, "show", [DOMElements.errorMsg]);
        }
    }

    function initListeners () {
        DOMElements.submitBtn.addEventListener("click", validate.bind(this));
        DOMElements.table.addEventListener("click", convertPass.bind(this));
        DOMElements.resetBtn.addEventListener("click", resetForm.bind(this));
    }


    function setFormData (form) {
        DOMElements = form;
    }

    this.initValidator = function(form) {
            initListeners.call(this);
    }

};

var formValidator = new FormValidator({
                                        name: document.querySelector("#inputName"),
                                        email: document.querySelector("#inputEmail"),
                                        password: document.querySelector("#inputPassword"),
                                        table: document.querySelector(".table"),
                                        tableContent: document.querySelector("#table-content"),
                                        submitBtn: document.querySelector("#submit"),
                                        resetBtn: document.querySelector("#reset"),
                                        successMsg: document.querySelector(".bg-success"),
                                        errorMsg: document.querySelector(".bg-danger"),
                                        form: document.querySelector("#form")
                                    });
formValidator.initValidator();
