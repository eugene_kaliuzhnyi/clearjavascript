/*Home work #2 task 3
Объявите переменную, но не присваивайте ей значение.
Выведите ее в консоль чтоб убедиться что она undefined.
Затем поочередно присваивайте ей разные значения и выводите в консоль.
Булевое значение -> в консоль, число -> в консоль, string -> в консоль, null -> в консоль.
И в конце проверьте переменную с null с помощью оператора typeof.*/
"use strict";

var viewType;
console.log(viewType);

viewType = true;
console.log(viewType);

viewType = 10;
console.log(viewType);

viewType = "some text";
console.log(viewType);

viewType = null;
console.log(viewType);

console.log (typeof(viewType));
