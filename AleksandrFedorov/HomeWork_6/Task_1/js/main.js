"use strict"
var btn = document.getElementById("play");

function getNewList(oldList, quantity) {
    var newList = [];
    oldList.forEach(function(carInfo, index) {

        if (index < quantity) {
            newList.push({                
                url: carInfo.url,
                name: carInfo.name,
                date: carInfo.date,
                params: carInfo.params,
                description: carInfo.description
            });
        }

    });

    return newList;
}

function changeName(name) {
    return name.charAt(0).toUpperCase() + name.substr(1).toLowerCase();
}

function modifyUrl(url) {
    return "http://" + url;
}

function modifyDescription(description) {
    return description.substr(0, 15) + "...";
}

function modifyDate(date) {
    var tmpDate = new Date(date);
    var year = tmpDate.getFullYear(),
        month = tmpDate.getMonth() + 1,
        day = tmpDate.getDate(),
        hours = tmpDate.getHours(),
        minutes = tmpDate.getMinutes();
    var addZero = function (partDate) {
        if (partDate < 10) {
            return "0" + partDate;
        } else {
            return partDate;
        }
    }
    month = addZero(month);
    day = addZero(day);
    hours = addZero(hours);
    minutes = addZero(minutes);
    return `${year}/${month}/${day} ${hours}:${minutes}`
}


function getChangedNewData(newData) {
    return newData.map(function(element, index) {
        return {
            name: changeName(element.name),
            url: modifyUrl(element.url),
            description: modifyDescription(element.description),
            date: modifyDate(element.date),
            params: `${element.params.status}=>${element.params.progress}`
        }
    });
}

var print = function (someText) {
    console.log(someText);
}

function transform() {
    var quantityElements = 5;
    var newData = getNewList(data, quantityElements);
    print(getChangedNewData(newData));

}

btn.addEventListener("click", transform);
