var first = document.querySelector('#first-line');
var second = document.querySelector('#second-line');
var third = document.querySelector('#third-line');

var myNewArr = editNewArr(data);

function modifyName(name) {
	return name[0] + name.substring(1).toLowerCase()
}
function modifyUrl(url) {
	return "http://" + url;
}

function modifyDescription(description) {
	return description.slice(0, 15) + "...";
}

function modifyDate(date) {
	var firstDate = new Date(date);
	function createNewDate(argument) {
		if(argument < 10) {
			return "0" + argument;
		} else {
			return argument;
		}
	}
	return firstDate.getFullYear() + "/" +
			createNewDate(firstDate.getMonth()+1) + "/" +
			createNewDate(firstDate.getDate()) + " " +
			createNewDate(firstDate.getHours()) + ":" +
			createNewDate(firstDate.getMinutes());

}
function modifyParams(params) {
	return params.status + "=>" + params.progress
}
function editNewArr(arr) {
	return arr.map(function (item, index) {
		return {
			name 				: modifyName(item.name),
			url 				: modifyUrl(item.url),
			description : modifyDescription(item.description),
			date 				: modifyDate(item.date),
			params 			: modifyParams(item.params)
		};
	})
}

function print(printText, elementId) {
	elementId.innerHTML += printText;
}

function replaceItems(item){
	var itemTemplate = 
	'<div class="col-sm-3 col-xs-6">\
		<img src="$url" alt="$name" class="img-thumbnail">\
		<div class="info-wrapper">\
			<div class="text-muted">$name</div>\
			<div class="text-muted">$description</div>\
			<div class="text-muted">$params</div>\
			<div class="text-muted">$date</div>\
			</div>\
	</div>';
	var result = "";
	result += itemTemplate
		.replace(/\$name/gi,item.name)
		.replace("$url", item.url)
		.replace("$params", item.params)
		.replace("$description", item.description)
		.replace("$date", item.date);
	return result;
}

function interpolateItems(item){
	var itemTemplate = `<div class="col-sm-3 col-xs-6">\
		<img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
			<div class="info-wrapper">\
				<div class="text-muted">${item.name}</div>\
				<div class="text-muted">${item.description}</div>\
				<div class="text-muted">${item.params}</div>\
				<div class="text-muted">${item.date}</div>\
			</div>\
		</div>`
	var result = "";
		result += itemTemplate;
	return result;
}

function createItems(items){
	var grid = document.createElement("div");
	grid.className = "col-sm-3 col-xs-6";

	var thumbnail = document.createElement("img");
	thumbnail.setAttribute("src", items.url);
	thumbnail.setAttribute("alt", items.name);
	thumbnail.className = "img-thumbnail";

	var info = document.createElement("div");
	info.className = "info-wrapper";

	var innerName = document.createElement("div");
	innerName.className = "text-muted";
	innerName.innerHTML =  items.name;
	innerParams = innerName.cloneNode(true);
	innerParams.innerHTML = items.params;
	innerDescription = innerName.cloneNode(true);
	innerDescription.innerHTML = items.description;
	innerDate = innerName.cloneNode(true);
	innerDate.innerHTML = items.date;

	info.appendChild(innerName);
	info.appendChild(innerDescription);
	info.appendChild(innerParams);
	info.appendChild(innerDate);
	grid.appendChild(thumbnail);
	grid.appendChild(info);

	return grid;

}

function buildGallery(arr){
	arr.forEach(function(item, index) {
		if (index <= 2){
		print(replaceItems(item), first);
		}else if(index >= 3 && index <= 5){
		print(interpolateItems(item), second);
		}else if (index >= 6 && index <= 8){
		print(createItems(item).outerHTML, third);
		}
	});
}

buildGallery(myNewArr);