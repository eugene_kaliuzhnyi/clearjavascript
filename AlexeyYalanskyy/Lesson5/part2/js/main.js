var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var result = document.getElementById("result");

function getPlayerResult() {
	return Math.floor((Math.random() * 3) + 1);
}

function getNameById() {
	var rnd = getPlayerResult();
	if (rnd == 1) {
		return "Камень";
	} else if (rnd == 2) {
		return "Ножницы";
	} if (rnd == 3) {
		return "Бумага";
	}
}

function determineWinner(first, second) {
	if (first == second) {
		return 0;
	} else if (first == 1 && second == 2) {
		return 1;
	} else if (first == 2 && second == 1) {
		return 2;
	} else if (first == 1 && second == 3) {
		return 2;
	} else if (first == 3 && second == 1) {
		return 1;
	} else if (first == 2 && second == 3) {
		return 1;
	} else if (first == 3 && second == 2) {
		return 2;
	}
}

function printResult(winner) {
	var plrintWin;
	switch(winner) {
		case 0:
		plrintWin = "Ничья";
	break;
		case 1:
		plrintWin = "Выиграл первый игрок";
	break;
		case 2:
		plrintWin = "Выиграл второй игрок";
	break;
	}
result.innerHTML = plrintWin;
}

function runGame() {
	var first = getPlayerResult();
	var	second = getPlayerResult();
	player1.innerHTML = getNameById(first);
	player2.innerHTML = getNameById(second);

	printResult(determineWinner(first,second));
}

btn.addEventListener("click", runGame);