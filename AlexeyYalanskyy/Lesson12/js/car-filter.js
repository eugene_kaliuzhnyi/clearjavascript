function CarFilter(form){
    Filter.apply(this, arguments);
}

CarFilter.prototype = {
    getYear : function() {
        this.resultObject.year = this.DOMElements.yearFilter.value;
    },
    getCondition : function() {
        var elem = this.DOMElements.conditionFilter,
            temp = [];
        
        for (var i = 0; i < elem.length; i++) {
            if (elem[i].checked) {                
                temp.push(elem[i].value);
            }
        }       
                
        this.resultObject.condition = temp;        
    },
    readValues : function(e){
        e.preventDefault();        
        this.getPrice();
        this.getDate();
        this.getYear();
        this.getCondition();
        this.printValues();
    }
}

inheritense(Filter, CarFilter);

var carFilter = new CarFilter({
    priceFilter  : document.querySelector("#priceFilter"),
	dateFilter 	 : document.querySelector("#dateFilter"),
    yearFilter  : document.querySelector("#yearFilter"),
    conditionFilter : document.querySelectorAll(".condition"),
	resultText   : document.querySelector("#result-text"),
    submitBtn    : document.querySelector("#submit-btn"),
});

carFilter.initListener();