var btn = document.getElementById("play");

function getNewArray(arr, count){
	var newArr = [];
	arr.forEach(function (item, index){
        if(index <= count - 1){
            newArr.push({
                url: item.url,
                name: item.name,
                params: item.params,
                description: item.description,
                date: item.date
            });
        }
	})
	return newArr;
}



function editName(name){
	return name[0].toUpperCase() + name.slice(1).toLowerCase();	
}
function editUrl(url){
	return "http://"+url;
}
function editDescriprion(description){
	return description.slice(0, 15) + "...";
}
function editParams(params){
	return params.status + "=>" + params.progress;
}


function editDate(date){
	var tmpDate = new Date(date);
	function getCorrectDate(val){
        if(val < 10){
            return "0" + val; 
        } else {return val;}
    }
	return tmpDate.getFullYear() + "/" +
	       getCorrectDate(tmpDate.getMonth() + 1)	+ "/"  +
           getCorrectDate(tmpDate.getDate()) + " " +
	       getCorrectDate(tmpDate.getHours()) + ":"	+
           getCorrectDate(tmpDate.getMinutes());
}


function transformNewArray(newArray){
	return newArray.map(function(item, index){
		return {
			name: editName(item.name),
			url: editUrl(item.url),
			description: editDescriprion(item.description),
			date: editDate(item.date),
			params: editParams(item.params)
		};
	})
   
}


function transform() {    
    var count = 7;            
    var result = transformNewArray(getNewArray(data, count));
    console.log(result);   
}



btn.addEventListener("click", transform);




