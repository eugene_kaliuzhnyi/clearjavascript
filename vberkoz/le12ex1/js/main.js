
(function() {

	function Filter(elements) {

		this.dataObject = {};

		this.DOMElements = {
			price : document.querySelector("#price"),
			date : document.querySelector("#date"),
			request : document.querySelector("#request"),
			button : document.querySelector(".btn")
		}

		for (var key in elements) {
			this.DOMElements[key] = elements[key];
		}

	}

	Filter.prototype = {

		listenButton : function() {
			this.DOMElements.button.addEventListener("click", this.generateRequest.bind(this));
		},

		getPrice : function() {
			this.dataObject.price = this.DOMElements.price.value;
		},

		getDate : function() {
			this.dataObject.date = this.DOMElements.date.value;
		},

		printValues : function() {
			this.DOMElements.request.innerHTML = JSON.stringify(this.dataObject);
		}

	}

	function ClockFilter(elements) {
		Filter.apply(this, arguments);
	}

	ClockFilter.prototype = {

		getStrap : function() {
			this.dataObject.strap = this.DOMElements.strap.value;
		},

		getMechanism : function() {
			var mechanismOption = this.DOMElements.mechanism;
			for (var i = 0; i < mechanismOption.length; i++) {
				if (mechanismOption[i].checked) {
					this.dataObject.mechanism = mechanismOption[i].value;
				}
			}
		},

		generateRequest : function() {
			this.getStrap();
			this.getMechanism();
			this.getPrice();
			this.getDate();
			this.printValues();
		}

	}

	function CarFilter(elements) {
		Filter.apply(this, arguments);
	}

	CarFilter.prototype = {

		getYear : function() {
			var year = this.DOMElements.year.value
			if (isFinite(year) && (!isNaN(parseInt(year)))) {
				this.dataObject.year = year;
			} else {
				delete this.dataObject.year;
				this.DOMElements.year.value = "";
			}
		},

		getBodyType : function() {
			var bodyTypeOption = this.DOMElements.bodyType;
			var bodyTypeArray = [];
			for (var i = 0; i < bodyTypeOption.length; i++) {
				if (bodyTypeOption[i].checked) {
					bodyTypeArray.push(bodyTypeOption[i].value);
				}
			}
			if (bodyTypeArray.length == 0) {
				delete this.dataObject.bodyType;
			} else {
				this.dataObject.bodyType = bodyTypeArray;
			}
		},

		generateRequest : function() {
			this.getYear();
			this.getBodyType();
			this.getPrice();
			this.getDate();
			this.printValues();
		}

	}

	function inheritense(parent, child) {

		var tempChild = child.prototype;

		if (Object.create) {
			child.prototype = Object.create(parent.prototype);
		} else {
			function F() {};
			F.prototype = parent.prototype;
			child.prototype = new F();
		}

		for (var key in tempChild) {
			if (tempChild.hasOwnProperty(key)) {
				child.prototype[key] = tempChild[key];
			}
		}

	}

	if (document.querySelector(".container").getAttribute("id") == "clock-screen") {

		inheritense(Filter, ClockFilter);

		var clockFilter = new ClockFilter({
			strap : document.querySelector("#strap-type"),
			mechanism : document.getElementsByName("mechanism-type")
		});

		clockFilter.listenButton();

	}

	if (document.querySelector(".container").getAttribute("id") == "car-screen") {

		inheritense(Filter, CarFilter);

		var carFilter = new CarFilter({
			year : document.querySelector("#year"),
			bodyType : document.getElementsByName("body-type")
		});

		carFilter.listenButton();

	}

})();
