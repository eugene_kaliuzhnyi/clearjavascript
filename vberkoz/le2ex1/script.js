//Lesson 2, exercise 1 homework
//
//Format code and find a mistake.
//(function(win){var params = {states:{url:»/»,template: «temlate.js»}}function setStates(params){if(win && !win.params){win.params = //params;}}setStates();console.log(params.states.template);})(window);

(function (win) {
    var params = {
        states: {
            url: "/",
            template: "temlate.js"
        }
    }; //Here was a mistake. Lack of semicolon.
    function setStates(params) {
        if (win && !win.params) {
            win.params = params;
        }
    }
    setStates();
    console.log(params.states.template);
})(window);