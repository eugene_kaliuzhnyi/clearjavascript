
//--------------------------------------------------------
//-               Character counter screen               -
//--------------------------------------------------------

var getText = document.getElementById('gettext');
var setText = document.getElementById('settext');
var btnReset = document.getElementById('reset');
var cntLetters = document.getElementById('count-letters');

//--------- Processing reset button
btnReset.addEventListener('click', function () {
    getText.value = '';
    trackCharacter();
})

//--------- Processing keyboard input
getText.addEventListener('input', trackCharacter);
getText.addEventListener('paste', trackCharacter);
function trackCharacter() {
    var maxLength = 200;
    var curLength = getText.value.length;
    if (curLength < maxLength) {
        setText.innerHTML = getText.value;
        cntLetters.innerHTML = curLength;
        cntLetters.style.color = 'black';
    } else if (curLength >= maxLength) {
        getText.value = getText.value.substr(0,maxLength);
        setText.innerHTML = getText.value;
        cntLetters.innerHTML = getText.value.length;
        cntLetters.style.color = 'red';
    }
}

//--------------------------------------------------------
//-                    Gallery screen                    -
//--------------------------------------------------------

//--------- Function for converting primary array
function convertData(incomingArray) {
    return incomingArray.map(function (item, index) {
        item.url         = newUrl(item.url);
        item.name        = newName(item.name);
        item.date        = newDate(item.date);
        item.params      = newParams(item.params);
        item.description = newDescription(item.description);
    })

    function newUrl(url) {return "http://" + url;}
    function newName(name) {return name[0].toLocaleUpperCase() + name.substring(1).toLowerCase();}
    function newParams(params) {return params.status + "=>" + params.progress;}
    function newDescription(description) {return description.substring(0, 15) + "...";}

    function newDate(dat){
        var tmpDate = new Date(dat);
        return fixDateString(tmpDate.getFullYear())     + "/" +
               fixDateString(tmpDate.getMonth() + 1)    + "/" +
               fixDateString(tmpDate.getDate())         + " " +
               fixDateString(tmpDate.getHours())        + ":" +
               fixDateString(tmpDate.getMinutes());
        function fixDateString(start) {return (start < 10) ? "0" + start : start;}
    }
}

//--------- Function for creating gallery item
function createGalleryItem(item, domElement) {
    var itemTemplate = `<div class="col-sm-3 col-xs-3">\
                            <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                            <div class="info-wrapper">\
                                <div class="text-muted">${item.name}</div>\
                                <div class="text-muted">${item.description}</div>\
                                <div class="text-muted">${item.params}</div>\
                                <div class="text-muted">${item.date}</div>\
                            </div>\
                            <button class="btn btn-danger" type="button" id="${item.id}">Удалить</button>\
                        </div>`;
    domElement.innerHTML += itemTemplate;
}

//--------- visibleItemsArray is used to display the correct sequence of photos
var visibleItemsArray = [];

//--------- Converting primary array from data.js
convertData(data);

//--------- Processing add photo button
var galleryArea = document.querySelector('#gallery-area');
var btnAddPhoto = document.getElementById('add-photo');
btnAddPhoto.addEventListener('click', function () {
    if (data.length > 0) {
        createGalleryItem(data[0], galleryArea);
        visibleItemsArray.unshift(data.shift());
        displayAvalPhoto();
    }
})

//--------- Processing remove photo button
var btnRemovePhoto = document.getElementById('gallery-area');
btnRemovePhoto.addEventListener('click', function (event) {
	var target = event.target;
	if (target.className == 'btn btn-danger') {

        // Searching for element with id property, which equals button id
        visibleItemsArray.map(function (item, index) {
            if (item.id == target.id) {
                data.push(visibleItemsArray[index]);
                visibleItemsArray.splice(index, 1);
            }
        })
		var child = target.parentNode;
		child.parentNode.removeChild(child);
        displayAvalPhoto();
	}
})

//--------- Displaying number of avaliable photos
var countPhoto = document.getElementById('count-photo');
function displayAvalPhoto() {
	countPhoto.innerHTML = data.length;
}
displayAvalPhoto();