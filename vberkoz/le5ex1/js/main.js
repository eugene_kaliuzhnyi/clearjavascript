//Task objective is to learn the basic techniques for working with functions.

//Create main function run() which will run the main code (cycle).
//Also, this function should call other functions.
var result = document.getElementById("result");
function run() {
    var total = 0;
    for (var i = 0; i < 15; i++) {
        if(i == 8 || i == 13){continue;}
        var first = getRndNumber();
        var second = getRndNumber();
        print("First dice: " + first + " Second dice: " + second + "<br/>", result);
        print(isNumbersEqual(first, second), result);
        print(isBigDifference(first, second), result);
        total += first + second;
    }
    print(calculateTotal(first, second, total), result);
}

//Make a function getRndNumber() to generate random numbers.
function getRndNumber() {
    return Math.floor((Math.random() * 6) + 1);
}

//Make a function print() that will print the line.
//It should only take one argument a row of text.
function print(msg) {
    result.innerHTML += msg;
}

//Make a function IsNumbersEqual() to determine matches.
//It should contain a check for a match and return the string "Fell double. Number: ".
function isNumbersEqual(firstNum, secondNum) {
    if(firstNum == secondNum) {
        return "Fell double. Number: " + firstNum + "<br/>";
    } else {
        return "";
    }
}

//Make a function IsBigDifference() to determine the difference.
//It must contain the appropriate verification and return the string "The large spread between dices. The difference is: "
function isBigDifference(firstNum, secondNum) {
    if(firstNum < 3 && secondNum > 4) {
        return "The large spread between dices. The difference is: " + (secondNum - firstNum) + "<br/>";
    } else if(secondNum < 3 && firstNum > 4) {
        return "The large spread between dices. The difference is: " + (firstNum - secondNum) + "<br/>";
    } else {
        return "";
    }
}

//Make a function to calculate the total result.
//The function should calculate the result and return it.
//Print resulting value using the print() function.
function calculateTotal(firstNum, secondNum, sum) {
    if(sum >= 100) {
      return "Victory! You have scored: " + sum + " points.";
    } else {
      return "Defeat! You have scored: " + sum + " points.";
    }
}

run();