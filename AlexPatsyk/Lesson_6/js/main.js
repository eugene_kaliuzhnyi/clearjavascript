var btn = document.getElementById("play");

function createNewArray(data, count) {
	var modified = [];
	data.forEach(function(item, index){
		if (count<=index && count<data.length) {
			modified.push({
				url : item.url,
				name : item.name,
				params : item.params,
				description : item.description,
				date : item.date
			});
		}
	})
	return modified;
}

function getModifiedArray(data) {
	return data.map(function(item){
		return {
			url:'http://'+ item.url,
			name:capitalizeFirstLetter(item.name.toLowerCase()),
			params:item.params.status+'=>'+item.params.progress,
			description:item.description.substring(0, 15) + '...',
			date:timestampToDate(item.date),
		}
	})
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function timestampToDate(timestamp) {
	var tmpDate = new Date(timestamp),
        year = tmpDate.getFullYear(),
        month = tmpDate.getMonth() + 1,
        day = tmpDate.getDate(),
        hours = tmpDate.getHours(),
        minutes = tmpDate.getMinutes();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    return year +'/'+ month+'/'+day +' '+ hours +':'+ minutes;
}

function printResult(obj) {
	console.log(obj);
}

function transform() {
    var count = 5;
	var modified = createNewArray(data, count);
	var result = getModifiedArray(modified);
	printResult(result);	
}

btn.addEventListener("click", transform);
