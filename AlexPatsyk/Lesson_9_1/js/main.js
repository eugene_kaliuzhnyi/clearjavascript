var clockWidget = (function(){
	var DOMElements = null,
        timers = [],
        timerCount = 1,
        runningTimer = 0,
        timerAlert = document.querySelector(".bg-success"),
        timerFormatError = document.querySelector(".bg-danger");

    function showTimerAlert(){
        timerAlert.classList.remove('hidden');
        runningTimer = 1;
    }

    function hideTimerAlert(){
        timerAlert.classList.add('hidden');
    }

    function showTimeErrorFormat(){
        timerFormatError.classList.remove('hidden');
    }

    function hideTimeErrorFormat(){
        timerFormatError.classList.add('hidden');
    }

    function inArray(needle) {
        return timers.some(function(i){
            return i==needle;
        });
    }

    function isTimeFormatCorrect(time){
        var re = /([01]\d|2[0-3]):([0-5]\d)/;
        return re.test(time);
    }

    function showClock(){
        setInterval( function() {
            var seconds = new Date().getSeconds(),
                minutes = new Date().getMinutes(),
                hours = new Date().getHours(),
                secondsHtml = document.getElementById('sec'),
                minutesHtml = document.getElementById('min'),
                hoursHtml = document.getElementById('hours');

            secondsHtml.innerHTML = (( seconds < 10 ? "0" : "" ) + seconds);
            minutesHtml.innerHTML = (( minutes < 10 ? "0" : "" ) + minutes);
            hoursHtml.innerHTML = (( hours < 10 ? "0" : "" ) + hours);
        }, 1000);

    }

    function runTimer(){
        var count = 0;
        setInterval(function(){
            timers.forEach(function(item, i, arr){
                var minutes = new Date().getMinutes(),
                    hours = new Date().getHours(),
                    arrItem = item.split(':');
                if( (hours==(arrItem[0]*1)) && (minutes==(arrItem[1]*1)) ){
                    showTimerAlert();
                    count++;
                }
                if(count==60) {
                    hideTimerAlert();
                    count = 0;
                }
            });
        },1000);
    }

    function removeTableRow(event){
        var target = event.target;
            row = target.closest('tr'),
            cellText = row.childNodes[1].innerHTML,
            arrayItem = timers.indexOf(cellText);

        timers.splice(arrayItem, 1);
        DOMElements.tableContent.removeChild(row);
    }

    function addTableRow(){
        timers.push(DOMElements.newTime.value);
        timerCount = timers.indexOf(DOMElements.newTime.value);

        var row = DOMElements.tableContent.insertRow(DOMElements.tableContent.rows.length);
        row.insertCell(0).innerHTML = timerCount+1;
        row.insertCell(1).innerHTML = DOMElements.newTime.value;
        row.insertCell(2).innerHTML = `<input type="button" class='btn btn-default show-pass' data-id=${timerCount} value='Remove'/>`;
    }

    function addNewTimer(e){
        e.preventDefault();
        if(DOMElements.newTime.value
           && isTimeFormatCorrect(DOMElements.newTime.value)
           && !inArray(DOMElements.newTime.value) ){
            addTableRow();
            hideTimeErrorFormat()
        } else {
            showTimeErrorFormat();
        }
    }

	function init() {
        showClock();
        DOMElements.timeSubmit.addEventListener("click", addNewTimer);
        runTimer();
        DOMElements.removeTimerButton.addEventListener("click", removeTableRow);
	}

	return {
        setElements : function(obj){
            DOMElements = obj
        },
		init: function(obj){
			init();
		}
	}

}())

clockWidget.setElements({
    newTime : document.getElementById('time'),
    timeSubmit : document.getElementById('time_submit'),
    tableContent : document.getElementById('table-content'),
    removeTimerButton: document.getElementById("timers-table")
});

clockWidget.init();
