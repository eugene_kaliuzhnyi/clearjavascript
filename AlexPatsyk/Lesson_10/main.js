var messenger = (function() {
    var count = 0,
        firstName = "",
        secondName = "";

    function setFirstName (fName) {
        firstName = "Mr. " + fName;
    };

    function setSecondName(sName) {
        secondName = sName;
    };

    function getMessageText() {
        var greetingMessage = "Welcome " + firstName + " ";
            greetingMessage += secondName + ".Glad to see you ";
        return greetingMessage;
    };

    function resetCount() {
        count = 0;
        console.log("Count is 0");
    }

    return {
        init: function(firstName, secondName){
            setFirstName(firstName);
            setSecondName(secondName);
        },
        sayHello: function(){
            console.log(getMessageText() + "Count " +(count++));
        }
    }

})()

messenger.init("John", "Smith");
messenger.sayHello();
