function inheritense (parent, child) {
    var tempChild = child.prototype;

    if (Object.create) {
        child.prototype = Object.create(parent.prototype);
    } else {
        function F() {};
        F.prototype = parent.prototype;
        child.prototype = new F();
    }

    for(var key in tempChild) {
        if (tempChild.hasOwnProperty(key)){
            child.prototype[key] = tempChild[key];
        }
    }
}


function Filter(form){
    this.DOMElements = form;
    this.resultObject = {};
}

Filter.prototype = {
    initListener : function(){
        this.DOMElements.submitBtn.addEventListener("click", this.readValues.bind(this) );
    },
    getPrice : function(){
        this.resultObject.price = this.DOMElements.priceFilter.value;
    },
    getDate : function(){
        this.resultObject.date = this.DOMElements.dateFilter.value;
    },    
    printValues : function(){
        this.DOMElements.resultText.value = JSON.stringify( this.resultObject );
    }
}
