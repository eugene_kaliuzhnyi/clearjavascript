(function(){
    
    var model = function() {
        
        function getData() {
            return $.get( "js/data.json", function( data ) {
                console.log("model");
                return data;
            })            
        }
        
        function saveData(data) {
            console.log("Data successfuly saved: " + data);
        }
        
        function updateData(data) {
            console.log("Data successfuly updated: " + data.counter);
            console.log(data);
        }
        
        return {
            getData : getData,
            saveData: saveData,
            updateData: updateData
        }
    }
    
    window.app = window.app || {};
    window.app.model = model();
    
}())
