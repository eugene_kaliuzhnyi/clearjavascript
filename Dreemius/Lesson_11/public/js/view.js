'use strict';
(function() {

    function Gallery (model) {
        
        this.DOMElements = {
            saveBtn: document.querySelector("#saveBtn"),
            refreshBtn: document.querySelector("#refreshBtn"),
        };
        this.model = model;
        this.init();
        this.saveDefer = $.Deferred();
        this.counter = 0;
        
        this.eventHolder = $({});
        this.updateEventName = "update";
        //this.updateEvent = $.Event( this.updateEventName );
    }
    
    Gallery.prototype = {
        
        init : function () {
            this.buildGallery();
            this.initListeners();
        },
        
        initListeners : function (){
            
            this.DOMElements.saveBtn.addEventListener("click", function(){
                this.saveDefer.resolve("msg");
            }.bind(this));
            
            this.DOMElements.refreshBtn.addEventListener("click", function(){
                this.eventHolder.trigger( this.updateEventName , [{counter: this.counter++}]);
            }.bind(this));
        }, 
        
        buildGallery : function () {
            console.log("build gallery");
            console.log(this.model);
        },
        
    }
    
    window.app = window.app || {};
    window.app.Gallery = Gallery;
    
}());
