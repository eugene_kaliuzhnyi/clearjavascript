﻿var total = 0;
var disp = document.getElementById("result");
for (var i = 0; i < 15 ; i++) {
    var first = Math.floor((Math.random() * 6) + 1);
    var second = Math.floor((Math.random() * 6) + 1);
    if (i == 8 || i == 13) {
        continue
    };
    total += first + second;
    disp.innerHTML += "Первая кость: " + first + " Вторая кость: " + second + "<br>";
    if (first == second) {
        var number = first;
        disp.innerHTML += "Выпал дубль. Число " + first + " <br>"
    }
    else if (first < 3 && second > 4) {
        disp.innerHTML += "Большой разброс между костями. Разница составляет " + (second - first) + "<br>"
    }
}
var score = total;
disp.innerHTML += total >= 100 ? "Победа, вы набрали " + score + " очков " : "Вы проиграли, у вас " + score + " очков";