'use strict';
var startClock = (function(){
	
	var elements = null;
	var data = [];
	
	function startTime() {
		var today = new Date();
		var hour = today.getHours();
		var minute = checkTime(today.getMinutes());
		var second = checkTime(today.getSeconds());
		var time = (hour + ':' + minute)
		elements.alarmDisplay.innerHTML =
		hour + ":" + minute + ":" + second;
		setTimeout(startTime, 1000);
		function checkTime(val) {
			if (val < 10){val = "0" + val};
			return val;
		};
		if(second == '00'){
			startAlarm (time);
		}
	}
	
	function validate (event){
		event.preventDefault();
		var test = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/;
		var inputTest = elements.setAlarm.value.match(test);
		if (inputTest){
			createData ()
		}
	}
	
	function createData (){
		if(checkAlarm()){
			data.push (elements.setAlarm.value);
			addNewTableLine(data[data.length - 1], data.length);
			elements.setAlarm.value = '';
		}
	}
	
	function checkAlarm (){
		for (var i = 0; i <= data.length; i++){
			if(elements.setAlarm.value === data[i]){
				return false;
			}
		}
		return true;
	}
	function addNewTableLine (alarm, number) {
		var row = `<tr class='setted' data-id='${number}'>\
					<th scope="row">${number}</th>\
					<td>${alarm}</td>\
					<td class='status'>Будильник установлен</td>\
					<td><button class="btn btn-default" data-id="${number}">Удалить будильник</button></td>\
					</tr>`; 
		elements.tableContent.innerHTML += row;
	}
	
	function startAlarm (timeVal) {
		var alarmStatusDiv = document.querySelectorAll('.status');
		for (var i = 0; i <= data.length - 1; i++){
			if (data[i] == timeVal){
				alarmStatusDiv[i].innerHTML = 'Будильник сработал';
				toggleWakeUpMsg ();
				setTimeout(toggleWakeUpMsg, 5000);
			}
		}
	}
	
	function toggleWakeUpMsg (){
		elements.wakeUpMsg.classList.toggle('show');
	}

	function deleteAlarm (event) {
		var click = event.target;
		var id = click.getAttribute('data-id');
		var trDel = click.closest('tr');
		var a;
		if(id){
			a = data.splice(id - 1, 1);
			trDel.remove(trDel);
		}
	}
	
	function initListeners() {
		elements.alarmAdd.addEventListener("click", validate);
		elements.tableContent.addEventListener("click", deleteAlarm);
	}
	
	return {
		setFormData : function(form){	
			elements = form;
		},
		initValidator: function(){
			initListeners();
			startTime();
		},		
	}
	
}())

startClock.setFormData({
	setAlarm: document.querySelector("#inputAlarm"),
	alarmDisplay: document.querySelector("#alarm"),
	tableContent: document.querySelector("#table-content"),
	alarmAdd: document.querySelector("#submit"),
	wakeUpMsg: document.querySelector('.bg-wakeup')
})
startClock.initValidator();