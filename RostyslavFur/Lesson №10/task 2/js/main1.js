'use strict';
function FormValidator (form){
	this.DOMElements = form;
	this.userData = [];
	this.toggleDiv = function(status){
		this.DOMElements.errorDiv.classList.toggle('showed', !status);
		this.DOMElements.successDiv.classList.toggle('showed', status);
		this.DOMElements.table.classList.toggle('showed', status);
		this.DOMElements.formDiv.classList.toggle('closed', status);
	};	
	this.hideDiv = function(form){
		form.classList.remove('showed');
	};
	this.checkEmail = function(email){
		var test = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
		return email.match(test)
	};
	this.resetForm = function(){
		this.DOMElements.name.value = '';
		this.DOMElements.email.value = '';
		this.DOMElements.password.value = '';
		this.toggleDiv(false);
		this.hideDiv(this.DOMElements.errorDiv);
	};
	this.createData = function(){
		var dataLength = this.userData.push({
			nameVal: this.DOMElements.name.value,
			emailVal: this.checkEmail(this.DOMElements.email.value),
			passwHidden: (this.DOMElements.password.value).replace(/./gi, '*'),
			passwVal: this.DOMElements.password.value,
			rowNumber: this.userData.length + 1
		});
		this.addNewTableLine(this.userData[dataLength - 1]);
	};
	this.addNewTableLine = function({nameVal, emailVal, passwHidden, rowNumber}){
		var row = `<tr>\
					<th id="count" scope="row">${rowNumber}</th>\
					<td>${nameVal}</td>\
					<td>${emailVal}</td>\
					<td>${passwHidden}</td>\
					<td><button class="btn btn-default" data-id="${rowNumber}" password-value="hide" >Показать пароль</button></td>\
					</tr>`; 
		this.DOMElements.tableContent.innerHTML += row;
	};
	this.validate = function(event){
		event.preventDefault();
		if(this.DOMElements.name.value && 
			this.checkEmail(this.DOMElements.email.value) && 
			this.DOMElements.password.value){
				this.toggleDiv(true);
				this.createData();
		}
		else {
			this.toggleDiv(false);
		}
	};
	this.showPassw = function (event){
		var target = event.target;
		var passwordDiv = target.parentNode.previousElementSibling;
		var id = (target.getAttribute('data-id')-1);
		var idButton = target.getAttribute('data-id');
		var display = target.getAttribute('password-value');
		if (idButton) {
			if (display === 'hide'){
				passwordDiv.innerHTML = this.userData[id].passwVal;
				target.innerHTML = 'Скрыть пароль';
				target.setAttribute('password-value', 'show');
			}
			else {
				passwordDiv.innerHTML = this.userData[id].passwHidden;
				target.innerHTML = 'Показать пароль';
				target.setAttribute('password-value', 'hide');
			}
		}
	};
	function inItListeners () {
		this.DOMElements.submitBtn.addEventListener("click", this.validate.bind(this));	
  		this.DOMElements.resetBtn.addEventListener("click", this.resetForm.bind(this));
		this.DOMElements.tableContent.addEventListener("click", this.showPassw.bind(this));
	};
	this.inItValidator = function(){
		inItListeners.call(this);
	};

}
var formValidator = new FormValidator({
	name 	: document.querySelector("#inputName"),
	email 	: document.querySelector("#inputEmail"),
	password : document.querySelector("#inputPassword"),
	formDiv: document.querySelector("#info"),
	tableContent : document.querySelector("#table-content"),
	table: document.querySelector("#table"),
	submitBtn: document.querySelector("#submit"),
	resetBtn: document.querySelector("#reset"),
	successDiv: document.querySelector("#success"),
	errorDiv: document.querySelector("#error"),
});
formValidator.inItValidator();