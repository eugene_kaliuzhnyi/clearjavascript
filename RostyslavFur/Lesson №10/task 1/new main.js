'use strict';
var msg = (function(){
	var firstName = '';
	var secondName = '';
	var count = 0;
	function setFirstName (firstName) {
        firstName = "Mr. " + firstName;    
    };

    function setSecondName (secondName) {
        secondName= secondName;    
    };
	
    function getMessageText () {
	var greetingMessage = "Welcome " + firstName + " ";
        greetingMessage += secondName + ". Glad to see you";
	return greetingMessage;		
    };

    function resetCount () {
        count = 0;
        console.log("Count is 0");
    };  

	return {
		init: function(firstName, secondName) {
			setFirstName(firstName);
			setSecondName(secondName);   
		},
		sayHello: function () {
			console.log(getMessageText() + ". Count " +(count++));
			console.log(getMessageText() + ". Count " +(count++));	
			resetCount();
		}
	};
}());
msg.init('John', 'Connor');
msg.sayHello();