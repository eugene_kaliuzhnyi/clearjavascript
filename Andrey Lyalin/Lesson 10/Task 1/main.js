
(function () {
    var count = 0;
    var firstName = "";
    var secondName = "";

    setFirstName = function(name) {
        this.firstName = "Mr. " + name;
    };

    setSecondName = function(name) {
        this.secondName= name;
    };

    init = function(fName, sName) {
        setFirstName(fName);
        setSecondName(sName);
    };
    getMessageText = function () {
        var greetingMessage = "Welcome " + firstName + " ";
        greetingMessage += secondName + ".Glad to see you ";
        return greetingMessage;
    };

    sayHello = function () {
        console.log(getMessageText() + "Count " +(count++));
    };
    resetCount = function () {
        count = 0;
        console.log("Count is 0");
    };

})();
init("John", "Smith");
sayHello();

/*
*Образец с ипользованием This
*
 function Messenger () {
 this.count = 0;
 this.firstName = "";
 this.secondName = "";

 this.setFirstName = function(firstName) {
 this.firstName = "Mr. " + firstName;
 };

 this.setSecondName = function(secondName) {
 this.secondName= secondName;
 };

 this.init = function(firstName, secondName) {
 this.setFirstName(firstName);
 this.setSecondName(secondName);
 };

 this.getMessageText = function () {
 var greetingMessage = "Welcome " + this.firstName + " ";
 greetingMessage += this.secondName + ".Glad to see you ";
 return greetingMessage;
 };

 this.sayHello = function () {
 console.log(this.getMessageText() + "Count " +(this.count++));
 };

 this.resetCount = function () {
 this.count = 0;
 console.log("Count is 0");
 }

 }
 messenger = new Messenger();
 messenger.init("John", "Smith");
 messenger.sayHello();*/